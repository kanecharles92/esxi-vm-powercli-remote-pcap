#Requires -Modules VMware.VimAutomation.Common

<#
# Name    : Get-VMvNICSwitchPortInfo.ps1
# Author  : Kane Charles

.SYNOPSIS
This function will return the switchport details associated with a particular vNIC

.DESCRIPTION
This function utilises information returned by the vCenter API as well as host information queried via esxcli to provide all possible information regarding vNIC(s)

.INPUTS
[VMware.VimAutomation.Types.NetworkAdapter[]] $vNIC
One or more vNIC(s) to get switchport details for

.OUTPUTS
[System.Management.Automation.PSObject[]] $vNICSwitchPortInfo
A representation of all switchport details for supplied vNIC(s)

.EXAMPLE (NOTE: You do not need to include the comma ',' like the below example if calling in this manner)
PS> Get-VMvNICSwitchPortInfo -vNIC (Get-VM asdf | Get-NetworkAdapter)

.EXAMPLE (NOTE: You must include the comma ',' before your vNICs otherwise the function will run inefficiently. This is why: https://stackoverflow.com/questions/29973212/pipe-complete-array-objects-instead-of-array-items-one-at-a-time)
PS> ,(Get-VM *asdf* | Get-NetworkAdapter) | Get-VMvNICSwitchPortInfo

.NOTES
- Works for 1 or more vNICs
- Works for an array of vNICs from any number of VMs

#>

function Get-VMvNICSwitchPortInfo
{
    [CmdletBinding()]
    [OutputType('System.Management.Automation.PSObject[]')]

    param
    (
        [ValidateNotNullOrEmpty()]
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [VMware.VimAutomation.Types.NetworkAdapter[]]$vNIC
    )

    Begin
    {
        $ErrorActionPreference = 'Stop'
        Write-Debug $MyInvocation.MyCommand.Name

        # Initialise array to store all PSCustomObject(s)
        $vNICSwitchPortInfo = @()
    }
    
    Process
    {
        # Get unique list of ESXi hosts from supplied vNICs(s)
        $uniqueESXiHosts = $vNIC | Sort-Object -Property {$_.Parent.VMHost} | Select-Object @{N="VMHost";E={$_.Parent.VMHost}} -Unique

        try
        {
            # For each host
            foreach ($ESXiHost in $uniqueESXiHosts)
            {
                # Get VMHost handle from current array index
                $currentHost = $ESXiHost.VMHost

                # Get esxcli handle
                $esxcli = $currentHost | Get-EsxCli -V2

                # For each vNIC on this host
                foreach ($vnic in ($vNIC | Where-Object {$_.Parent.VMHost -eq $currentHost} | Sort-Object -Property Parent))
                {
                    # Get VM world id
                    $vmWorldId = ($esxcli.network.vm.list.invoke() | Where-Object {$_.Name -eq $vnic.Parent.Name}).WorldID

                    # Create spec to query this host for the network information of this vm
                    $arguments = $esxcli.network.vm.port.list.CreateArgs()

                    # Set world id
                    $arguments.worldid = $vmWorldID

                    # Query for port(s) information
                    $portInfo = $esxcli.network.vm.port.list.Invoke($arguments)

                    # Get the relevant entry from $portInfo
                    $currentvNICInfo = $portInfo | Where-Object {$_.DVPortID -eq $vnic.extensionData.Backing.Port.PortKey}
                    
                    # Create a PSCustomObject containing the relevant properties
                    $object = [pscustomobject]@{
                        Name          = $vnic.Name
                        Parent        = $vnic.Parent.Name
                        MacAddress    = $vnic.MacAddress
                        PortgroupName = $vnic.NetworkName
                        PortID        = $currentvNICInfo.PortID
                        PortInfo      = $currentvNICInfo
                        TeamUplink    = $currentvNICInfo.TeamUplink
                        Type          = $vnic.Type
                        UplinkPortID  = $currentvNICInfo.UplinkPortID
                        VM            = $vnic.Parent
                        VMHost        = $vnic.Parent.VMHost
                        vNIC          = $vnic
                        vSwitch       = $currentvNICInfo.vSwitch
                    }
    
                    # Create a unique object typename
                    $object.PSObject.TypeNames.Insert(0,'vNIC.PortInfo')

                    # Configure a default display set
                    $defaultDisplaySet = 'Name','VM','VMHost','MacAddress','PortgroupName','PortID','TeamUplink','Type','UplinkPortID','vSwitch'

                    # Create the default property display set
                    $defaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet',[string[]]$defaultDisplaySet)

                    # Create member info object
                    $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($defaultDisplayPropertySet)

                    # Add new member set
                    $object | Add-Member MemberSet PSStandardMembers $PSStandardMembers

                    # Add cuyrrent PSCustomObject to array
                    $vNICSwitchPortInfo += $object
                }
            }
        }
        catch [Exception] {
            Write-Host $_.Exception.Message
            throw "Unable to retrieve port(s) info"
        }
    }

    End
    {
        # Sort output
        $vNICSwitchPortInfo = $vNICSwitchPortInfo | Sort-Object -Property VM,Name

        # Display output
        Write-Output $vNICSwitchPortInfo
    }
}
