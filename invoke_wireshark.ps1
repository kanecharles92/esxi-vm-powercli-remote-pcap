param
(
    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$false
    )]
    [String]$PlinkPath = "C:\Program Files\PuTTY\plink.exe",

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$false
    )]
    [String]$ESXiUsername = "root",

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$ESXiPassword,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$VMHostAddress,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$SwitchPortID,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$false
    )]
    [String]$WireSharkPath = "C:\Program Files\Wireshark\wireshark.exe",

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$VMName,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$vNICName,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$vNICMacAddress,

    [ValidateNotNullOrEmpty()]
    [Parameter(
        Mandatory=$true
    )]
    [String]$PortgroupName,

    [ValidateSet('Ingress','Egress','Bidirectional')]
    [Parameter(
        Mandatory=$true
    )]
    [String]$Direction,

    [Parameter(
        Mandatory=$false
    )]
    [String]$CaptureFilter = ""
)

# Define constants for direction
$dir_Egress = '0'
$dir_Ingress = '1'
$dir_Bidirectional = '2'

$acceptKeyCommand = "echo y `| `"$($PlinkPath)`" -l $($ESXiUsername) -pw $($ESXiPassword) -P 22 $($VMHostAddress) `"exit`""

Write-Host $acceptKeyCommand

$invokeWiresharkCommand = "`"$($PlinkPath)`" -batch -l $($ESXiUsername) -pw $($ESXiPassword) -P 22 $($VMHostAddress) pktcap-uw --dir $((Get-Variable -Name ('dir_' + $Direction)).Value) --switchport $($SwitchPortID) $($CaptureFilter) -o - `| `"$($WireSharkPath)`" -o `"gui.window_title:$($Direction.ToUpper()) - $($VMName) - $($vNICName) - $($vNICMacAddress) -  $($SwitchPortID) - $($PortgroupName) - $($VMHostAddress)`" -k -i - &"

Write-Host $invokeWiresharkCommand

# Accept SSH key first
Invoke-Command -ScriptBlock {& cmd.exe /c "$($acceptKeyCommand)"}

# Run command
Invoke-Command -ScriptBlock {& cmd.exe /c "$($invokeWiresharkCommand)"}
