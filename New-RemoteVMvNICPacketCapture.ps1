#Requires -Modules VMware.VimAutomation.Common

<#
# Name    : New-RemoteVMvNICPacketCapture.ps1
# Author  : Kane Charles

.SYNOPSIS
This function will initiate a remote wireshark capture for a given vNIC(s)

.DESCRIPTION
This function does the following:
- Determine which host(s) said vNIC(s) are on
- Enable SSH for said host(s)
- Create pktcap-uw session, writing to stdout, feed into Wireshark instance(s) on local machine

.INPUTS
[VMware.VimAutomation.Types.NetworkAdapter[]] $vNIC
One or more vNICs to get 

.OUTPUTS
[System.Management.Automation.PSObject[]] $New-RemoteVMvNICPacketCapture
A representation of all switchport details for supplied VM(s)

.EXAMPLE (NOTE: You do not need to include the comma ',' like the below example if calling in this manner)
PS> New-RemoteVMvNICPacketCapture -vNIC (Get-VM asdf | Get-NetworkAdapter)

.EXAMPLE (NOTE: You must include the comma ',' before your vNICs otherwise the function will run inefficiently. This is why: https://stackoverflow.com/questions/29973212/pipe-complete-array-objects-instead-of-array-items-one-at-a-time)
PS> ,(Get-VM *asdf* | Get-NetworkAdapter) | New-RemoteVMvNICPacketCapture

.NOTES
- Works for 1 or more VMs
- Works for 1 or more vNICs on a given VM

#>

function New-RemoteVMvNICPacketCapture
{
    [CmdletBinding()]

    param
    (
        [ValidateNotNullOrEmpty()]
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [VMware.VimAutomation.Types.NetworkAdapter[]]$vNIC,
        
        [ValidateNotNullOrEmpty()]
        [Parameter(
            Mandatory=$true
        )]
        [System.Management.Automation.PSCredential]$ESXiRootCredential,

        [ValidateNotNullOrEmpty()]
        [Parameter(
            Mandatory=$false
        )]
        [String]$WireSharkPath = 'C:\Program Files\Wireshark\wireshark.exe',

        [ValidateNotNullOrEmpty()]
        [Parameter(
            Mandatory=$false
        )]
        [String]$PlinkPath = 'C:\Program Files\PuTTY\plink.exe',

        [ValidateSet('Ingress','Egress','Bidirectional')]
        [Parameter(
            Mandatory=$false
        )]
        [String]$Direction = 'Bidirectional',

        [Parameter(
            Mandatory=$false
            )]
        [String]$CaptureFilter = ""
    )

    Begin
    {
        $ErrorActionPreference = 'Stop'
        Write-Debug $MyInvocation.MyCommand.Name

        # Import function needed to get switch port ID(s)
        try {
            . "$PSScriptRoot\Get-VMvNICSwitchPortInfo.ps1"
        }
        catch {
            Write-Error "Unable to import function Get-VMvNICSwitchPortInfo, exiting!"
            break
        }
    }
    
    Process
    {
        try {
            # Get unique list of ESXi hosts from supplied vNICs(s)
            $uniqueESXiHosts = $vNIC | Sort-Object -Property {$_.Parent.VMHost} | Select-Object @{N="VMHost";E={$_.Parent.VMHost}} -Unique

            Write-Host "uniqueESXiHosts: $($uniqueESXiHosts)" -ForegroundColor Yellow

            # For each host
            foreach ($ESXiHost in $uniqueESXiHosts)
            {
                # Get VMHost handle from current array index
                $currentHost = $ESXiHost.VMHost

                # Determine whether to do override direction based on ESXi version (6.7 can only do one direction at a time)
                # ie 6.5.0 becomes 650, 6.7.0 becomes 670
                $hostVersion = $currentHost.Version -replace "\.",""

                # Get SSH service
                $sshService = $currentHost | Get-VMHostService | Where-Object {$_.Key -eq 'TSM-SSH'}

                Write-Host "INITIAL SSH STATE: $($sshService.Running)" -ForegroundColor Yellow

                # Start SSH if not started
                if ($sshService.Running -ne $true)
                {
                    $sshService | Start-VMHostService
                }

                Start-Sleep 2

                Write-Host "POST UPDATE STATE: $(($currentHost | Get-VMHostService | Where-Object {$_.Key -eq 'TSM-SSH'}).Running)" -ForegroundColor Yellow

                # Test that SSH is reachable
                if (-not (Test-NetConnection -Port 22 -ComputerName $currentHost))
                {
                    Write-Error "Unable to connect to \"$currentHost\" on port 22. Skipping the following VMs: " +  + ([String]::Join(', ',($vNIC | Where-Object {$_.Parent.VMHost -eq $currentHost} | Sort-Object -Property Parent).Parent.Name))
                    break
                }
                else {
                    Write-Host "Able to connect to $currentHost on port 22"
                }

                # For each vNIC on this host
                foreach ($vnic in ($vNIC | Where-Object {$_.Parent.VMHost -eq $currentHost} | Sort-Object -Property Parent))
                {
                    # Get switch port ID
                    $currentvNICSwitchPortInfo = $vnic | Get-VMvNICSwitchPortInfo
                    Write-Host $currentvNICSwitchPortInfo

                    if ($hostVersion -lt 670 -and $Direction -eq "Bidirectional")
                    {
                        $ingressArgs = "-File `"$($PSScriptRoot)\invoke_wireshark.ps1`" -ESXiPassword $($ESXiRootCredential.GetNetworkCredential().password) -VMHostAddress $($currentHost.Name) -SwitchPortID $($currentvNICSwitchPortInfo.portID) -VMName $($currentvNICSwitchPortInfo.VM.Name) -vNICName `"$($currentvNICSwitchPortInfo.Name)`" -vNICMacAddress `"$($currentvNICSwitchPortInfo.MacAddress)`" -PortgroupName $($currentvNICSwitchPortInfo.PortgroupName) -Direction Ingress -CaptureFilter `"$($CaptureFilter)`""

                        Write-Host $ingressArgs -ForegroundColor Yellow
                        Start-Process -FilePath "powershell" -ArgumentList $ingressArgs

                        $egressArgs = "-File `"$($PSScriptRoot)\invoke_wireshark.ps1`" -ESXiPassword $($ESXiRootCredential.GetNetworkCredential().password) -VMHostAddress $($currentHost.Name) -SwitchPortID $($currentvNICSwitchPortInfo.portID) -VMName $($currentvNICSwitchPortInfo.VM.Name) -vNICName `"$($currentvNICSwitchPortInfo.Name)`" -vNICMacAddress `"$($currentvNICSwitchPortInfo.MacAddress)`" -PortgroupName $($currentvNICSwitchPortInfo.PortgroupName) -Direction Egress -CaptureFilter `"$($CaptureFilter)`""

                        Write-Host $egressArgs -ForegroundColor Yellow
                        Start-Process -FilePath "powershell" -ArgumentList $egressArgs
                    }
                    else
                    {
                        $myargs = "-File `"$($PSScriptRoot)\invoke_wireshark.ps1`" -ESXiPassword $($ESXiRootCredential.GetNetworkCredential().password) -VMHostAddress $($currentHost.Name) -SwitchPortID $($currentvNICSwitchPortInfo.portID) -VMName $($currentvNICSwitchPortInfo.VM.Name) -vNICName `"$($currentvNICSwitchPortInfo.Name)`" -vNICMacAddress `"$($currentvNICSwitchPortInfo.MacAddress)`" -PortgroupName $($currentvNICSwitchPortInfo.PortgroupName) -Direction $($Direction) -CaptureFilter `"$($CaptureFilter)`""

                        Write-Host $myargs -ForegroundColor Yellow
                        Start-Process -FilePath "powershell" -ArgumentList $myargs
                    }
                }    

                # # If SSH was not running before running this function, stop it again
                # if ($sshService.Running -eq $false)
                # {
                #     $sshService | Stop-VMHostService -Confirm:$false
                # }
            }
        }
        catch [Exception] {
            Write-Host $_.Exception.Message
            throw "Unable to establish remote pcap"
        }
    }

    End
    {
    }
}
